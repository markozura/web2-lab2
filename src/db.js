const {Pool} = require('pg')

const pool = new Pool ({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    port: 5432,
    password: process.env.DB_PASSWORD,
    database: 'movies_lab2'
});

module.exports = pool