var router = require('express').Router();
const { requiresAuth } = require('express-openid-connect');
const pool = require('../db');

let protec = false;
let sqlInjection = false;

router.get('/', function (req, res, next) {
  res.render('index', {
    sql: sqlInjection,
    title: 'Auth0 Webapp sample Nodejs',
    isAuthenticated: req.oidc.isAuthenticated()
  });
});

router.get('/user', requiresAuth(), function (req, res, next) {
  res.render('profile', {
    userProfile: JSON.stringify(req.oidc.user, null, 2),
    title: 'Profile page'
  });
});

router.get('/admin', requiresAuth(), function (req, res, next) {
  if(req.oidc.user.email === "markozura@windowslive.com" || protec === true) {
    res.render('admin', {
      user: req.oidc.user,
      bac: protec
    });
  } else {
    res.render('notAdmin');
  }
});

router.post('/', requiresAuth(), function (req, res, next) {
  if(req.body.method === "baccheck") {
    if (req.body.control === "true") protec = true;
    else {
      protec = false;
    }
    res.render('index', {
      title: 'Auth0 Webapp sample Nodejs',
      isAuthenticated: req.oidc.isAuthenticated(),
      sql: sqlInjection
    });
  } else if (req.body.method === "idcheck") {
      if(sqlInjection === false) {
        if (isNaN(req.body.movieid)) {
          res.render('sqlInjectionReject');
        } else {
          pool.query(`select *
                      from movie
                      where id_movie = ${req.body.movieid}`, (error, result) => {
            if (error) throw error;
            else {
              res.render('movies', {
                movies: result.rows,
                user: req.oidc.user,
                sql: sqlInjection
              });
            }
          });
        }
      } else {
        pool.query(`select *
                      from movie
                      where id_movie = ${req.body.movieid}`, (error, result) => {
          if (error) throw error;
          else {
            res.render('movies', {
              movies: result.rows,
              user: req.oidc.user,
              sql: sqlInjection
            });
          }
        });
      }
  } else if(req.body.method === "sqlcheck") {
    if(req.body.control2 === "true") sqlInjection = true;
    else sqlInjection = false;
    res.render('index', {
      title: 'Auth0 Webapp sample Nodejs',
      isAuthenticated: req.oidc.isAuthenticated(),
      sql: sqlInjection
    });
  }
});

module.exports = router;
